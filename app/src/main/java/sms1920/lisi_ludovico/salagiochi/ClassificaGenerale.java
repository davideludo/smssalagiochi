package sms1920.lisi_ludovico.salagiochi;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.lisi_ludovico.salagiochi.R;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class ClassificaGenerale extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener{
    DrawerLayout drawer;
    NavigationView navigationView;
    ListView v;
    FirebaseAuth mAuth = FirebaseAuth.getInstance();
    FirebaseFirestore db=FirebaseFirestore.getInstance();
    FirebaseUser user = mAuth.getCurrentUser();
    String usernamemail="";
    Toolbar toolbar=null;
    String nickname;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_classifica_generale);
        v=findViewById(R.id.listaclassifica);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        if(user==null) {
            Intent i;
            i = getIntent();
            nickname = i.getStringExtra("nick");
            usernamemail=nickname;
        }else{
            usernamemail=user.getEmail();
        }
        Logged();
        db.collection("utenti").get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if (task.isSuccessful()) {
                    HashMap<String,Double> rankings=new HashMap<>();
                    for (QueryDocumentSnapshot document : task.getResult()) {
                        Double rank=0.0;
                        int bubble_score,snake_score,tetris_score,pong_nwins,pong_nloses,spinv_score,spinv_level;
                        try { bubble_score = document.getLong("bubble_score").intValue(); } catch(Exception e){ bubble_score=0;}
                        try{ snake_score=document.getLong("snake_score").intValue(); }catch(Exception e){snake_score=0;}
                        try{ tetris_score=document.getLong("tetris_score").intValue(); }catch(Exception e){tetris_score=0;}
                        try{ pong_nwins=document.getLong("pong_nwins").intValue(); }catch(Exception e){pong_nwins=0;}
                        try{ pong_nloses=document.getLong("pong_nloses").intValue(); }catch(Exception e){pong_nloses=0;}
                        try{ spinv_score=document.getLong("spinv_score").intValue();
                            spinv_level=document.getLong("spinv_level").intValue();
                        }catch(Exception e){spinv_score=0;spinv_level=0;}

                        rank=(bubble_score/10.0)+snake_score+tetris_score+((spinv_score/500.0)*spinv_level);
                        if(pong_nloses+pong_nwins!=0) {
                            rank += (pong_nwins * 10.0) / pong_nloses + pong_nwins;
                        }

                        rankings.put(document.getString("email"),rank);

                    }
                    List<Map.Entry<String,Double>> listOfEntry = new LinkedList<>(rankings.entrySet());

                    //Sort listOfEntry using Collections.sort() by passing customized Comparator

                    Collections.sort(listOfEntry, new Comparator<Map.Entry<String,Double>>()
                    {

                        @Override
                        public int compare(Map.Entry<String,Double> o1, Map.Entry<String,Double> o2)
                        {
                            return o2.getValue().compareTo(o1.getValue());
                        }
                    });

                    //Insert all elements of listOfEntry into new LinkedHashMap which maintains insertion order

                    Map<String,Double> sortedRankings = new LinkedHashMap<String,Double>();

                    for (Map.Entry<String,Double> entry : listOfEntry)
                    {
                        sortedRankings.put(entry.getKey(), entry.getValue());
                    }

                    List<String> list=new ArrayList<>();
                    int i=1;
                    for(Map.Entry<String, Double> entry : sortedRankings.entrySet()){
                        DecimalFormat df = new DecimalFormat("#.00");
                        String nums = df.format(entry.getValue());
                        list.add(i+"."+entry.getKey()+" - "+nums);
                        i++;
                    }
                    v.setAdapter(new ArrayAdapter<String>(getApplicationContext(),R.layout.list_classifica, list));

                } else {
                }
            }
        });
    }

    @SuppressLint("RestrictedApi")
    public void Logged(){

        FirebaseFirestore firebaseFirestore = FirebaseFirestore.getInstance();
        CollectionReference utenti = firebaseFirestore.collection("utenti");

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        navigationView = findViewById(R.id.nav_view);

        View headerView = navigationView.getHeaderView(0);
        final TextView header = headerView.findViewById(R.id.textView12);
        if(user!=null){
            utenti.whereEqualTo("email", usernamemail).get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                @Override
                public void onComplete(@NonNull Task<QuerySnapshot> task) {
                    if (task.isSuccessful()) {
                        for (QueryDocumentSnapshot document : task.getResult()) {
                            String headerst = document.getString("email");
                            header.setText(headerst);
                        }
                    }
                }
            });
        }else{
            String headers = usernamemail;
            header.setText(headers);
        }
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        //here is the main place where we need to work on.
        int id=item.getItemId();
        Intent i;
        switch (id){
            case R.id.nav_home:
                i = new Intent(this, Homepage.class);
                if(user==null) {
                    i.putExtra("nick", nickname);
                }
                startActivity(i);
                break;
            case R.id.nav_logout:
                mAuth.signOut();
                CharSequence text = getString(R.string.APresto);
                Toast.makeText(getApplicationContext(), text, Toast.LENGTH_SHORT).show();
                i = new Intent(this, sms1920.lisi_ludovico.salagiochi.MainActivity.class);
                startActivity(i);
                break;

            case R.id.nav_score:
                i = new Intent(this, CondivisionePunteggio.class);
                if(user==null) {
                    i.putExtra("nick", nickname);
                }
                startActivity(i);
                break;

            case R.id.nav_ranking:

                break;
        }
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) this.getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().isConnected();
    }
}
