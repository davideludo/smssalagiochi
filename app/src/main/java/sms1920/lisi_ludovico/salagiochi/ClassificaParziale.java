package sms1920.lisi_ludovico.salagiochi;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.lisi_ludovico.salagiochi.R;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import sms1920.lisi_ludovico.salagiochi.pong.PongOb;
import sms1920.lisi_ludovico.salagiochi.space_invaders.SpinvOb;

public class ClassificaParziale extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener{
    DrawerLayout drawer;
    NavigationView navigationView;
    Toolbar toolbar=null;
    FirebaseAuth mAuth = FirebaseAuth.getInstance();
    FirebaseUser user = mAuth.getCurrentUser();
    FirebaseFirestore db=FirebaseFirestore.getInstance();
    HashMap<String, SpinvOb> spinvHashMap;
    HashMap<String, PongOb> pongHashMap;
    HashMap<String, Integer> scoreMap;
    List<String> list;
    TextView titolo;
    ListView v;
    String nickname,usernamemail;
    private SQLiteDatabase mDB = null;
    private DBOpenHelper mDbHelper;
    int gamescore=0;
    int pongwins=0;
    int pongloses=0;
    int spinvlevel=0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_classifica_parziale);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        Intent i;
        i=getIntent();
        final String NomeGioco= i.getStringExtra("nomegioco");
        if(user==null) {
            nickname = i.getStringExtra("nick");
            usernamemail=nickname;

            if(Objects.equals(NomeGioco, getString(R.string.Tetris))){
                try {
                    mDbHelper = new DBOpenHelper(this.getApplicationContext());
                    mDB = mDbHelper.getReadableDatabase();
                    Cursor c = mDB.rawQuery("SELECT " + DBOpenHelper.SCORETETRIS + " FROM " + DBOpenHelper.TABLE_TETRIS + " WHERE " + DBOpenHelper.USER_MAIL_TETRIS + " = '" + usernamemail + "'", null);
                    c.moveToNext();
                    gamescore = Integer.parseInt(c.getString(c.getColumnIndex(DBOpenHelper.SCORETETRIS)));
                }catch(Exception e){}
                Toast.makeText(getApplicationContext(),getResources().getString(R.string.your_score)+gamescore, Toast.LENGTH_SHORT).show();
            }else if(Objects.equals(NomeGioco, getString(R.string.bubble_dodge))){
                try {
                    mDbHelper = new DBOpenHelper(this.getApplicationContext());
                    mDB = mDbHelper.getReadableDatabase();
                    Cursor c = mDB.rawQuery("SELECT " + DBOpenHelper.SCOREBUBBLE + " FROM " + DBOpenHelper.TABLE_BUBBLE + " WHERE " + DBOpenHelper.USER_MAIL_BUBBLE + " = '" + usernamemail + "'", null);
                    c.moveToNext();
                    gamescore = Integer.parseInt(c.getString(c.getColumnIndex(DBOpenHelper.SCOREBUBBLE)));
                } catch (Exception e) {}
                Toast.makeText(getApplicationContext(),getResources().getString(R.string.your_score)+gamescore, Toast.LENGTH_SHORT).show();
            }else if(Objects.equals(NomeGioco, getString(R.string.Pong))){
                try {
                    mDbHelper = new DBOpenHelper(this.getApplicationContext());
                    mDB = mDbHelper.getReadableDatabase();
                    Cursor c = mDB.rawQuery("SELECT * FROM " + DBOpenHelper.TABLE_PONG + " WHERE " + DBOpenHelper.USER_MAIL_PONG + " = '" + usernamemail + "'", null);
                    c.moveToNext();
                    pongwins = Integer.parseInt(c.getString(c.getColumnIndex(DBOpenHelper.NWIN)));
                    pongloses = Integer.parseInt(c.getString(c.getColumnIndex(DBOpenHelper.NLOSE)));
                }catch(Exception e){}
                Toast.makeText(getApplicationContext(),getResources().getQuantityString(R.plurals.numberOfVictories,pongwins,pongwins)+" "+getResources().getQuantityString(R.plurals.numberOfDefeats,pongloses,pongloses), Toast.LENGTH_SHORT).show();
            }else if(Objects.equals(NomeGioco, getString(R.string.SpaceInvaders))){
                try {
                    mDbHelper = new DBOpenHelper(this.getApplicationContext());
                    mDB = mDbHelper.getReadableDatabase();
                    Cursor c = mDB.rawQuery("SELECT * FROM " + DBOpenHelper.TABLE_SPINV + " WHERE " + DBOpenHelper.USER_MAIL_SPINV + " = '" + usernamemail + "'", null);
                    c.moveToNext();
                    gamescore = Integer.parseInt(c.getString(c.getColumnIndex(DBOpenHelper.SCORESPINV)));
                    spinvlevel = Integer.parseInt(c.getString(c.getColumnIndex(DBOpenHelper.LEVELSPINV)));
                }catch(Exception e){}
                Toast.makeText(getApplicationContext(),getResources().getString(R.string.your_level)+spinvlevel+", "+getResources().getString(R.string.score)+" "+gamescore, Toast.LENGTH_SHORT).show();

            }else if(Objects.equals(NomeGioco, getString(R.string.Snake))){
                try {
                    mDbHelper = new DBOpenHelper(this.getApplicationContext());
                    mDB = mDbHelper.getReadableDatabase();
                    Cursor c = mDB.rawQuery("SELECT " + DBOpenHelper.SCORESNAKE + " FROM " + DBOpenHelper.TABLE_SNAKE + " WHERE " + DBOpenHelper.USER_MAIL_SNAKE + " = '" + usernamemail + "'", null);
                    c.moveToNext();
                    gamescore = Integer.parseInt(c.getString(c.getColumnIndex(DBOpenHelper.SCORESNAKE)));
                }catch(Exception e){}
                Toast.makeText(getApplicationContext(),getResources().getString(R.string.your_score)+gamescore, Toast.LENGTH_SHORT).show();
            }

        }else{
            usernamemail=user.getEmail();
        }
        Logged();
        list=new ArrayList<>();
        titolo = findViewById(R.id.textView9);
        v = findViewById(R.id.listaclassificaparz);

        if(user==null){
            nickname = i.getStringExtra("nick");
        }

        titolo.setText(NomeGioco);
        if(NomeGioco.equals(getString(R.string.Tetris))){
            titolo.setTextColor(Color.parseColor("#FFFF8800"));
        }else if(NomeGioco.equals(getString(R.string.bubble_dodge))){
            titolo.setTextColor(Color.parseColor("#CC8EFFBC"));
        }else if(NomeGioco.equals(getString(R.string.Pong))){
            titolo.setTextColor(Color.parseColor("#FFFFFFFF"));
        }else if(NomeGioco.equals(getString(R.string.SpaceInvaders))){
            titolo.setTextColor(Color.parseColor("#FF33B5E5"));
        }else if(NomeGioco.equals(getString(R.string.Snake))){
            titolo.setTextColor(Color.parseColor("#FF669900"));
        }

        if(NomeGioco.equals(getString(R.string.SpaceInvaders))){
            spinvHashMap=new HashMap<>();
        }else if(NomeGioco.equals(getString(R.string.Pong))){
            pongHashMap=new HashMap<>();
        }else{
            scoreMap=new HashMap<>();
        }



        db.collection("utenti").get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if (task.isSuccessful()) {
                    for (QueryDocumentSnapshot document : task.getResult()) {
                        int rank=0;
                        if(Objects.equals(NomeGioco, getString(R.string.Tetris))){
                            try{ rank=(document.getLong("tetris_score").intValue());
                                scoreMap.put(document.getString("email"),rank);
                            }catch(Exception e){}

                        }else if(Objects.equals(NomeGioco, getString(R.string.Pong))){
                            PongOb scores=new PongOb();
                            try{scores.setNwins(document.getLong("pong_wins").intValue()); }catch(Exception e){scores.setNwins(0);}
                            try{scores.setNloses(document.getLong("pong_loses").intValue()); }catch(Exception e){scores.setNloses(0);}
                            if(!scores.isNull())pongHashMap.put(document.getString("email"),scores);
                        }else if(Objects.equals(NomeGioco, getString(R.string.SpaceInvaders))){
                            try{
                                SpinvOb spinv_scores=new SpinvOb(document.getLong("spinv_score").intValue(),document.getLong("spinv_level").intValue());
                                spinvHashMap.put(document.getString("email"),spinv_scores);
                            }catch(Exception e){}
                        }else if(Objects.equals(NomeGioco, getString(R.string.Snake))){
                            try{ rank=(document.getLong("snake_score").intValue());
                                scoreMap.put(document.getString("email"),rank);
                            }catch(Exception e){}
                        }else if(Objects.equals(NomeGioco, getString(R.string.bubble_dodge))){
                            try{ rank=(document.getLong("bubble_score").intValue());
                                scoreMap.put(document.getString("email"),rank);
                            }catch(Exception e){}
                        }
                    }
                    if(Objects.equals(NomeGioco, getString(R.string.Pong))){sortPongScores();}
                    else if(Objects.equals(NomeGioco, getString(R.string.SpaceInvaders))){sortSpinvScores();}
                    else {sortScores();}
                    v.setAdapter(new ArrayAdapter<String>(getApplicationContext(),R.layout.list_classifica, list));
                } else {
                }
            }
        });
    }

    @SuppressLint("RestrictedApi")
    public void Logged(){

        FirebaseFirestore firebaseFirestore = FirebaseFirestore.getInstance();
        CollectionReference utenti = firebaseFirestore.collection("utenti");

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        navigationView = findViewById(R.id.nav_view);

        View headerView = navigationView.getHeaderView(0);
        final TextView header = headerView.findViewById(R.id.textView12);
        if(user!=null) {
            utenti.whereEqualTo("email", usernamemail).get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                @Override
                public void onComplete(@NonNull Task<QuerySnapshot> task) {
                    if (task.isSuccessful()) {
                        for (QueryDocumentSnapshot document : task.getResult()) {
                            String headerst = document.getString("email");
                            header.setText(headerst);
                        }
                    }
                }
            });
        }else{
            String headers = usernamemail;
            header.setText(headers);
        }
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        //here is the main place where we need to work on.
        int id=item.getItemId();
        Intent i;
        switch (id){
            case R.id.nav_home:
                i = new Intent(this, Homepage.class);
                if(user==null) {
                    i.putExtra("nick", nickname);
                }
                startActivity(i);
                break;
            case R.id.nav_logout:
                mAuth.signOut();
                CharSequence text = getString(R.string.APresto);
                Toast.makeText(getApplicationContext(), text, Toast.LENGTH_SHORT).show();
                i = new Intent(this, sms1920.lisi_ludovico.salagiochi.MainActivity.class);
                startActivity(i);
                break;

            case R.id.nav_score:
                i = new Intent(this, CondivisionePunteggio.class);
                if(user==null) {
                    i.putExtra("nick", nickname);
                }
                startActivity(i);
                break;

            case R.id.nav_ranking:
                i = new Intent(this, ClassificaGenerale.class);
                if(user==null) {
                    i.putExtra("nick", nickname);
                }
                startActivity(i);
                break;
        }
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void sortPongScores(){
        List<Map.Entry<String,PongOb>> listOfEntry = new LinkedList<>(pongHashMap.entrySet());

        //Sort listOfEntry using Collections.sort() by passing customized Comparator

        Collections.sort(listOfEntry, new Comparator<Map.Entry<String,PongOb>>()
        {

            @Override
            public int compare(Map.Entry<String,PongOb> o1, Map.Entry<String,PongOb> o2)
            {
                return o2.getValue().compareTo(o1.getValue());
            }
        });

        //Insert all elements of listOfEntry into new LinkedHashMap which maintains insertion order

        Map<String,PongOb> sortedMap = new LinkedHashMap<String,PongOb>();

        for (Map.Entry<String,PongOb> entry : listOfEntry)
        {
            sortedMap.put(entry.getKey(), entry.getValue());
        }
        int i=1;
        for(Map.Entry<String, PongOb> entry : sortedMap.entrySet()){

            list.add(i+"."+entry.getKey()+" - "+ getResources().getQuantityString(R.plurals.numberOfVictories,entry.getValue().getNwins(),entry.getValue().getNwins())
            +", "+getResources().getQuantityString(R.plurals.numberOfDefeats,entry.getValue().getNloses(),entry.getValue().getNloses()));
            i++;
        }
    }

    public void sortSpinvScores(){
        List<Map.Entry<String,SpinvOb>> listOfEntry = new LinkedList<>(spinvHashMap.entrySet());

        //Sort listOfEntry using Collections.sort() by passing customized Comparator

        Collections.sort(listOfEntry, new Comparator<Map.Entry<String,SpinvOb>>()
        {

            @Override
            public int compare(Map.Entry<String,SpinvOb> o1, Map.Entry<String,SpinvOb> o2)
            {
                return o2.getValue().compareTo(o1.getValue());
            }
        });

        //Insert all elements of listOfEntry into new LinkedHashMap which maintains insertion order

        Map<String,SpinvOb> sortedMap = new LinkedHashMap<String,SpinvOb>();

        for (Map.Entry<String,SpinvOb> entry : listOfEntry)
        {
            sortedMap.put(entry.getKey(), entry.getValue());
        }
        int i=1;
        for(Map.Entry<String, SpinvOb> entry : sortedMap.entrySet()){

            list.add(i+"."+entry.getKey()+" - "+getResources().getString(R.string.level)+" "+entry.getValue().getLevel()
                    +", "+getResources().getString(R.string.score)+" "+entry.getValue().getScore());
            i++;
        }
    }
    public void sortScores(){
        List<Map.Entry<String,Integer>> listOfEntry = new LinkedList<>(scoreMap.entrySet());

        //Sort listOfEntry using Collections.sort() by passing customized Comparator

        Collections.sort(listOfEntry, new Comparator<Map.Entry<String,Integer>>()
        {

            @Override
            public int compare(Map.Entry<String,Integer> o1, Map.Entry<String,Integer> o2)
            {
                return o2.getValue().compareTo(o1.getValue());
            }
        });

        //Insert all elements of listOfEntry into new LinkedHashMap which maintains insertion order

        Map<String,Integer> sortedMap = new LinkedHashMap<String,Integer>();

        for (Map.Entry<String,Integer> entry : listOfEntry)
        {
            sortedMap.put(entry.getKey(), entry.getValue());
        }
        int i=1;
        for(Map.Entry<String, Integer> entry : sortedMap.entrySet()){

            list.add(i+"."+entry.getKey()+" - "+getResources().getString(R.string.score)+" "+entry.getValue());
            i++;
        }
    }
    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) this.getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().isConnected();
    }
}
