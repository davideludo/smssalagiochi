package sms1920.lisi_ludovico.salagiochi;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import android.annotation.SuppressLint;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothServerSocket;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.lisi_ludovico.salagiochi.R;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.util.Set;
import java.util.UUID;

public class CondivisionePunteggio extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener{
    DrawerLayout drawer;
    NavigationView navigationView;
    FirebaseAuth mAuth = FirebaseAuth.getInstance();
    FirebaseUser user = mAuth.getCurrentUser();
    Toolbar toolbar=null;

    String nickname;
    boolean flag=false;
    private SQLiteDatabase mDB = null;
    int bestScoretetris=0;
    int bestScorebubble=0;
    int bestScoresnake=0;
    int bestLevel=0;
    int bestScore=0;
    int nwin=0;
    int nlose=0;
    boolean sent;
    String to, usernamemail;
    private TextView btnSend, btnListen, btnListDevice;
    private TextView status, msg_box, risultato;
    private ListView listViewBT;
    private BluetoothAdapter bluetoothAdapter;

    private SendReceive sendReceive;
    BluetoothDevice[] btArray;

    private static final int STATE_LINTENING = 1;
    private static final int STATE_CONNECTING = 2;
    private static final int STATE_CONNECTED = 3;
    private static final int STATE_CONNECTION_FAILED = 4;
    private static final int STATE_MESSAGE_RECEIVED = 5;

    int REQUEST_ENABLE_BLUETOOTH = 1000;

    private static final String APP_NAME = "GamesRoom";
    private static final UUID MY_UUID = UUID.fromString("8ce255c0-223a-11e0-ac64-0803450c9a66");

    private double punteggiototale = 0;

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_condivisione_punteggio);

        if(user==null) {
            Intent i;
            i = getIntent();
            nickname = i.getStringExtra("nick");
            usernamemail=nickname;
        }else{
            usernamemail=user.getEmail();
        }

        Logged();

        inizializzaUI();
        sent=false;
        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

        if(!bluetoothAdapter.isEnabled())
        {
            Intent enableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableIntent, REQUEST_ENABLE_BLUETOOTH);
        }

        implementListenrs();

        DBOpenHelper dbOpenHelper = new DBOpenHelper(this);
        mDB = dbOpenHelper.getReadableDatabase();
        try {
            Cursor c = mDB.rawQuery("SELECT " + DBOpenHelper.SCORETETRIS + " FROM " + DBOpenHelper.TABLE_TETRIS + " WHERE " + DBOpenHelper.USER_MAIL_TETRIS + " = '" + usernamemail + "'", null);
            c.moveToNext();
            bestScoretetris = Integer.parseInt(c.getString(c.getColumnIndex(DBOpenHelper.SCORETETRIS)));
        }catch(Exception e){}
        try {
            Cursor a = mDB.rawQuery("SELECT " + DBOpenHelper.SCOREBUBBLE + " FROM " + DBOpenHelper.TABLE_BUBBLE + " WHERE " + DBOpenHelper.USER_MAIL_BUBBLE + " = '" + usernamemail + "'", null);
            a.moveToNext();
            bestScorebubble = Integer.parseInt(a.getString(a.getColumnIndex(DBOpenHelper.SCOREBUBBLE)));
        }catch(Exception e){}
        try {
            Cursor b = mDB.rawQuery("SELECT " + DBOpenHelper.SCORESNAKE + " FROM " + DBOpenHelper.TABLE_SNAKE + " WHERE " + DBOpenHelper.USER_MAIL_SNAKE + " = '" + usernamemail + "'", null);
            b.moveToNext();
            bestScoresnake = Integer.parseInt(b.getString(b.getColumnIndex(DBOpenHelper.SCORESNAKE)));
        }catch(Exception e){}
        try {
            Cursor d = mDB.rawQuery("SELECT * FROM " + DBOpenHelper.TABLE_SPINV + " WHERE " + DBOpenHelper.USER_MAIL_SPINV + " = '" + usernamemail + "'", null);
            d.moveToNext();
            bestLevel = Integer.parseInt(d.getString(d.getColumnIndex(DBOpenHelper.LEVELSPINV)));
            bestScore = Integer.parseInt(d.getString(d.getColumnIndex(DBOpenHelper.SCORESPINV)));
        }catch(Exception e){}
        try {
            Cursor e = mDB.rawQuery("SELECT * FROM " + DBOpenHelper.TABLE_PONG + " WHERE " + DBOpenHelper.USER_MAIL_PONG + " = '" + usernamemail + "'", null);
            e.moveToNext();
            nlose = Integer.parseInt(e.getString(e.getColumnIndex(DBOpenHelper.NLOSE)));
            nwin = Integer.parseInt(e.getString(e.getColumnIndex(DBOpenHelper.NWIN)));
            mDB.close();
        }catch(Exception e){ }
        punteggiototale=bestScoresnake+bestScoretetris+(bestScorebubble/10.0)+((bestScore/500.0)*bestLevel);
        if(nwin+nlose!=0){
            punteggiototale+=+((nwin*10.0)/nwin+nlose);
        }
    }

    private void implementListenrs() {
        btnListDevice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Set<BluetoothDevice> bt = bluetoothAdapter.getBondedDevices();
                String[] strings = new String[bt.size()];
                btArray = new BluetoothDevice[bt.size()];
                int index = 0;

                if (bt.size() > 0){

                    for(BluetoothDevice device : bt){
                        btArray[index] = device;
                        strings[index] = device.getName();
                        index++;
                    }
                    ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(getApplicationContext(),R.layout.list_bluetooth, strings);
                    listViewBT.setAdapter(arrayAdapter);
                }
            }
        });

        btnListen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(!bluetoothAdapter.isEnabled()){
                    Intent enableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                    startActivityForResult(enableIntent, REQUEST_ENABLE_BLUETOOTH);
                    ServerClass serverClass = new ServerClass();
                    serverClass.start();
                }else{
                    ServerClass serverClass = new ServerClass();
                    serverClass.start();
                }
            }
        });

        listViewBT.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ClientClass clientClass = new ClientClass(btArray[position]);
                clientClass.start();
                CharSequence text = getString(R.string.Connecting);
                status.setText(text);
            }
        });


        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!flag){
                    try {
                        sendReceive.write(String.valueOf(punteggiototale).getBytes());
                        sent = true;
                        flag=true;
                    }catch (Exception e){
                        CharSequence text11 = getString(R.string.NotConnected);
                        Toast.makeText(getApplicationContext(), text11, Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
    }

    Handler handler = new Handler(new Handler.Callback() {
        @SuppressLint("SetTextI18n")
        @Override
        public boolean handleMessage(@NonNull Message msg) {
            switch(msg.what) {
                case STATE_LINTENING:
                    status.setText(getText(R.string.Listening));
                    break;

                case STATE_CONNECTING:
                    status.setText(getText(R.string.Connecting));
                    break;

                case STATE_CONNECTED:
                    status.setText(getText(R.string.Connected)+to);
                    break;

                case STATE_CONNECTION_FAILED:
                    status.setText(getText(R.string.ConnectionFailed));
                    break;

                case STATE_MESSAGE_RECEIVED:
                    byte[] readBuff = (byte[]) msg.obj;
                    String tempMsg = new String(readBuff, 0,msg.arg1);
                    Double a=Double.parseDouble(tempMsg);
                    @SuppressLint("DefaultLocale") String b = String.format("%.2f",a);
                    msg_box.setText(msg_box.getText().toString().concat(" ").concat(b));
                    listViewBT.setVisibility(View.GONE);
                    msg_box.setVisibility(View.VISIBLE);
                    risultato.setVisibility(View.VISIBLE);
                    TextView testo = findViewById(R.id.testosend);
                    if(!sent) {
                        testo.setVisibility(View.VISIBLE);
                    }
                    if(punteggiototale>Float.parseFloat(tempMsg)){
                        risultato.setText(getText(R.string.GreaterAverage));
                    }else if(punteggiototale<Float.parseFloat(tempMsg)){
                        risultato.setText(getText(R.string.MinorAverage));
                    }else {
                        risultato.setText(R.string.TieScore);
                    }

                    break;
            }
            return true;
        }
    });

    public void inizializzaUI(){
        btnSend = findViewById(R.id.send);
        btnListen = findViewById(R.id.listen);
        btnListDevice = findViewById(R.id.listDevice);
        listViewBT = findViewById(R.id.lista_dispositivi);
        status = findViewById(R.id.status);
        msg_box = findViewById(R.id.msg_box);
        risultato = findViewById(R.id.risultato);
    }

    private class ServerClass extends Thread {
        private BluetoothServerSocket serverSocket;
        public ServerClass() {
            try{
                serverSocket = bluetoothAdapter.listenUsingInsecureRfcommWithServiceRecord(APP_NAME, MY_UUID);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        public void run()
        {
            BluetoothSocket socket = null;while(socket == null)
            {
                try {
                    Message message = Message.obtain();
                    message.what = STATE_CONNECTING;
                    handler.sendMessage(message);
                    socket = serverSocket.accept();
                } catch (IOException e) {
                    e.printStackTrace();
                    Message message = Message.obtain();
                    message.what = STATE_CONNECTION_FAILED;
                    handler.sendMessage(message);
                }

                if(socket != null)
                {
                    Message message = Message.obtain();
                    message.what = STATE_CONNECTED;
                    to=socket.getRemoteDevice().getName();
                    handler.sendMessage(message);

                    sendReceive = new SendReceive(socket);
                    sendReceive.start();

                    break;
                }
            }
        }
    }

    private class ClientClass extends Thread {
        private BluetoothDevice device;
        private  BluetoothSocket socket;

        public ClientClass(BluetoothDevice device1)
        {
            device = device1;

            try {
                socket = device.createRfcommSocketToServiceRecord(MY_UUID);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        public void run()
        {
            try {
                socket.connect();
                Message message = Message.obtain();
                message.what = STATE_CONNECTED;
                to=device.getName();
                handler.sendMessage(message);

                sendReceive = new SendReceive(socket);
                sendReceive.start();

            } catch (IOException e) {
                e.printStackTrace();
                Message message = Message.obtain();
                message.what = STATE_CONNECTION_FAILED;
                handler.sendMessage(message);
            }

        }
    }

    private class SendReceive extends Thread {
        private final BluetoothSocket bluetoothSocket;
        private final InputStream inputStream;
        private final OutputStream outputStream;

        private SendReceive (BluetoothSocket socket)
        {
            bluetoothSocket = socket;
            InputStream tempIn = null;
            OutputStream tempOut = null;

            try {
                tempIn = bluetoothSocket.getInputStream();
                tempOut = bluetoothSocket.getOutputStream();
            } catch (IOException e) {
                e.printStackTrace();
            }

            inputStream = tempIn;
            outputStream = tempOut;

        }

        public void run()
        {
            byte[] buffer = new byte[1024];
            int bytes;

            while(true)
            {
                try {
                    bytes=inputStream.read(buffer);
                    handler.obtainMessage(STATE_MESSAGE_RECEIVED, bytes, -1, buffer).sendToTarget();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }


        public void write(byte[] bytes)
        {
            try {
                outputStream.write(bytes);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @SuppressLint("RestrictedApi")
    public void Logged(){

        FirebaseFirestore firebaseFirestore = FirebaseFirestore.getInstance();
        CollectionReference utenti = firebaseFirestore.collection("utenti");

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        navigationView = findViewById(R.id.nav_view);

        View headerView = navigationView.getHeaderView(0);
        final TextView header = headerView.findViewById(R.id.textView12);
        if(user!=null) {
            utenti.whereEqualTo("email", user.getEmail()).get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                @Override
                public void onComplete(@NonNull Task<QuerySnapshot> task) {
                    if (task.isSuccessful()) {
                        for (QueryDocumentSnapshot document : task.getResult()) {
                            String headerst = document.getString("email");
                            header.setText(headerst);
                        }
                    }
                }
            });
        }else{
            String headers = usernamemail;
            header.setText(headers);
        }
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        //here is the main place where we need to work on.
        int id=item.getItemId();
        Intent i;
        switch (id){
            case R.id.nav_home:
                i = new Intent(this, Homepage.class);
                if(user==null) {
                    i.putExtra("ospite", true);
                    i.putExtra("nick", nickname);
                }
                startActivity(i);
                break;
            case R.id.nav_logout:
                mAuth.signOut();
                CharSequence text = getString(R.string.APresto);
                Toast.makeText(getApplicationContext(), text, Toast.LENGTH_SHORT).show();
                i = new Intent(this, sms1920.lisi_ludovico.salagiochi.MainActivity.class);
                startActivity(i);
                break;

            case R.id.nav_score:

                break;

            case R.id.nav_ranking:
                i = new Intent(this, ClassificaGenerale.class);
                if(user==null) {
                    i.putExtra("ospite", true);
                    i.putExtra("nick", nickname);
                }
                startActivity(i);
                break;
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
