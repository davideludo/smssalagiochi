package sms1920.lisi_ludovico.salagiochi;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import sms1920.lisi_ludovico.salagiochi.tetris.TetrisCtrl;

public class DBOpenHelper extends SQLiteOpenHelper {

    public final static String TABLE_TETRIS = "tetris";
    public final static String ID_TETRIS = "id";
    public final static String USER_MAIL_TETRIS = "email";
    public final static String SCORETETRIS = "score";

    public final static String TABLE_BUBBLE = "BubbleDodge";
    public final static String ID_BUBBLE = "id";
    public final static String USER_MAIL_BUBBLE = "email";
    public final static String SCOREBUBBLE = "score";

    public final static String TABLE_PONG = "Pong";
    public final static String ID_PONG = "id";
    public final static String USER_MAIL_PONG = "email";
    public final static String NWIN = "nwin";
    public final static String NLOSE = "nlose";

    public final static String TABLE_SNAKE = "Snake";
    public final static String ID_SNAKE = "id";
    public final static String USER_MAIL_SNAKE = "email";
    public final static String SCORESNAKE = "score";

    public final static String TABLE_SPINV = "SpaceInvaders";
    public final static String ID_SPINV = "id";
    public final static String USER_MAIL_SPINV = "email";
    public final static String SCORESPINV = "score";
    public final static String LEVELSPINV = "level";


    public final static String[] columns_Tetris = {ID_TETRIS, USER_MAIL_TETRIS, SCORETETRIS};
    public final static String[] columns_Bubble = {ID_BUBBLE, USER_MAIL_BUBBLE, SCOREBUBBLE};
    public final static String[] columns_Pong = {ID_PONG,USER_MAIL_PONG,NWIN,NLOSE};
    public final static String[] columns_Snake = {ID_SNAKE,USER_MAIL_SNAKE,SCORESNAKE};
    public final static String[] columns_Spinv = {ID_SPINV,USER_MAIL_SPINV,SCORESPINV,LEVELSPINV};



    final private static String CREATE_CMD_TETRIS =
            "CREATE TABLE " + TABLE_TETRIS + "("
                    + ID_TETRIS + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                    + USER_MAIL_TETRIS + " TEXT, " + SCORETETRIS + " INTEGER)";

    final private static String CREATE_CMD_BUBBLE =
            "CREATE TABLE " + TABLE_BUBBLE + "("
                    + ID_BUBBLE + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                    + USER_MAIL_BUBBLE + " TEXT, " + SCOREBUBBLE + " INTEGER)";

    final private static String CREATE_CMD_PONG =
            "CREATE TABLE " + TABLE_PONG + "("
                    + ID_PONG + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                    + USER_MAIL_PONG + " TEXT, "+NWIN+" INTEGER, " + NLOSE + " INTEGER)";

    final private static String CREATE_CMD_SNAKE =
            "CREATE TABLE " + TABLE_SNAKE + "("
                    + ID_SNAKE + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                    + USER_MAIL_SNAKE + " TEXT, " + SCORESNAKE + " INTEGER)";

    final private static String CREATE_CMD_SPINV =
            "CREATE TABLE " + TABLE_SPINV + "("
                    + ID_SPINV + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                    + USER_MAIL_SPINV + " TEXT, " + SCORESPINV + " INTEGER, "+ LEVELSPINV+" INTEGER)";


    //final private static String DBNAME = "student_db";
    final private static String DBNAME = "db_games.db";
    final private static Integer VERSION = 1;
    final private Context mContext;

    public DBOpenHelper(Context context) {
        super(context, DBNAME, null, VERSION);
        this.mContext = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_CMD_TETRIS);
        db.execSQL(CREATE_CMD_BUBBLE);
        db.execSQL(CREATE_CMD_PONG);
        db.execSQL(CREATE_CMD_SNAKE);
        db.execSQL(CREATE_CMD_SPINV);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // N/A
    }

    void deleteDatabase() {
        mContext.deleteDatabase(DBNAME);
    }
}

