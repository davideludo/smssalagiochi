package sms1920.lisi_ludovico.salagiochi;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.viewpager.widget.ViewPager;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.lisi_ludovico.salagiochi.R;

import sms1920.lisi_ludovico.salagiochi.bubble_dodge.BubbleDodgeMain;
import sms1920.lisi_ludovico.salagiochi.pong.Pong;
import sms1920.lisi_ludovico.salagiochi.snake.Snake;
import sms1920.lisi_ludovico.salagiochi.space_invaders.MainActivity;
import sms1920.lisi_ludovico.salagiochi.tetris.Tetris;

public class Homepage extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener{
    DrawerLayout drawer;
    NavigationView navigationView;
    FirebaseAuth mAuth = FirebaseAuth.getInstance();
    FirebaseUser user = mAuth.getCurrentUser();
    Toolbar toolbar=null;
    private SQLiteDatabase mDB = null;
    private DBOpenHelper mDbHelper;
    int gamescore=0;
    int spinvlevel=0;
    int pongwins=0;
    int pongloses=0;
    String nickname;
    String usernamemail;
    Double punteggiototale;
    int scoresnake, scoretetris, scorebubble, scorespace;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_homepage);

        ViewPager viewPager = findViewById(R.id.viewpager);
        viewPager.setAdapter(new CustomPagerAdapter(this));

        if(user==null) {
            Intent i;
            i = getIntent();
            nickname = i.getStringExtra("nick");
            usernamemail=nickname;
        }else{
            usernamemail=user.getEmail();
        }

        Logged();
    }
    public void Tetris (View view){
        Intent i = new Intent(Homepage.this, Tetris.class);
        if(user==null){
            i.putExtra("nick",nickname);
        }
        startActivity(i);
    }

    public void Pong (View view){
        Intent i = new Intent(Homepage.this, Pong.class);
        if(user==null){
            i.putExtra("nick",nickname);
        }
        startActivity(i);
    }

    public void Snake (View view){
        Intent i = new Intent(Homepage.this, Snake.class);
        if(user==null){
            i.putExtra("nick",nickname);
        }
        startActivity(i);
    }

    public void SpaceInvaders (View view){
        Intent i = new Intent(Homepage.this, MainActivity.class);
        if(user==null){
            i.putExtra("nick",nickname);
        }
        startActivity(i);
    }

    public void BubbleDodge (View view){
        Intent i = new Intent(Homepage.this, BubbleDodgeMain.class);
        if(user==null){
            i.putExtra("nick",nickname);
        }
        startActivity(i);
    }

    public void ClassificaTetris (View view){
        if(!isNetworkConnected()){
            try {
                mDbHelper = new DBOpenHelper(this.getApplicationContext());
                mDB = mDbHelper.getReadableDatabase();
                Cursor c = mDB.rawQuery("SELECT " + DBOpenHelper.SCORETETRIS + " FROM " + DBOpenHelper.TABLE_TETRIS + " WHERE " + DBOpenHelper.USER_MAIL_TETRIS + " = '" + usernamemail + "'", null);
                c.moveToNext();
                gamescore = Integer.parseInt(c.getString(c.getColumnIndex(DBOpenHelper.SCORETETRIS)));
            }catch(Exception e){gamescore=0;}
            Toast.makeText(getApplicationContext(),getResources().getString(R.string.your_score)+gamescore, Toast.LENGTH_SHORT).show();
        }else {
            Intent i = new Intent(Homepage.this, ClassificaParziale.class);
            CharSequence text = getString(R.string.Tetris);
            i.putExtra("nomegioco", text);
            if(user==null){
                i.putExtra("nick", nickname);
            }
            startActivity(i);
        }
    }

    public void ClassificaBubble (View view) {
        if (!isNetworkConnected()) {
            try {
                mDbHelper = new DBOpenHelper(this.getApplicationContext());
                mDB = mDbHelper.getReadableDatabase();
                Cursor c = mDB.rawQuery("SELECT " + DBOpenHelper.SCOREBUBBLE + " FROM " + DBOpenHelper.TABLE_BUBBLE + " WHERE " + DBOpenHelper.USER_MAIL_BUBBLE + " = '" + usernamemail + "'", null);
                c.moveToNext();
                gamescore = Integer.parseInt(c.getString(c.getColumnIndex(DBOpenHelper.SCOREBUBBLE)));
            } catch (Exception e) {gamescore=0;}
            Toast.makeText(getApplicationContext(),getResources().getString(R.string.your_score)+gamescore, Toast.LENGTH_SHORT).show();
        } else {
            Intent i = new Intent(Homepage.this, ClassificaParziale.class);
            CharSequence text1 = getString(R.string.bubble_dodge);
            i.putExtra("nomegioco", text1);
            if(user==null){
                i.putExtra("nick", nickname);
            }
            startActivity(i);
        }
    }
    public void ClassificaSnake (View view){
        if(!isNetworkConnected()){
            try {
                mDbHelper = new DBOpenHelper(this.getApplicationContext());
                mDB = mDbHelper.getReadableDatabase();
                Cursor c = mDB.rawQuery("SELECT " + DBOpenHelper.SCORESNAKE + " FROM " + DBOpenHelper.TABLE_SNAKE + " WHERE " + DBOpenHelper.USER_MAIL_SNAKE + " = '" + usernamemail + "'", null);
                c.moveToNext();
                gamescore = Integer.parseInt(c.getString(c.getColumnIndex(DBOpenHelper.SCORESNAKE)));
            }catch(Exception e){gamescore=0;}
            Toast.makeText(getApplicationContext(),getResources().getString(R.string.your_score)+gamescore, Toast.LENGTH_SHORT).show();
        }else {
            Intent i = new Intent(Homepage.this, ClassificaParziale.class);
            CharSequence text2 = getString(R.string.Snake);
            i.putExtra("nomegioco", text2);
            if(user==null){
                i.putExtra("nick", nickname);
            }
            startActivity(i);
        }
        }


    public void ClassificaSpace (View view){

        if(!isNetworkConnected()){
            try {
                mDbHelper = new DBOpenHelper(this.getApplicationContext());
                mDB = mDbHelper.getReadableDatabase();
                Cursor c = mDB.rawQuery("SELECT * FROM " + DBOpenHelper.TABLE_SPINV + " WHERE " + DBOpenHelper.USER_MAIL_SPINV + " = '" + usernamemail + "'", null);
                c.moveToNext();
                gamescore = Integer.parseInt(c.getString(c.getColumnIndex(DBOpenHelper.SCORESPINV)));
                spinvlevel = Integer.parseInt(c.getString(c.getColumnIndex(DBOpenHelper.LEVELSPINV)));
            }catch(Exception e){gamescore=0;}
            Toast.makeText(getApplicationContext(),getResources().getString(R.string.your_level)+spinvlevel+", "+getResources().getString(R.string.score)+" "+gamescore, Toast.LENGTH_SHORT).show();
        }else {
            Intent i = new Intent(Homepage.this, ClassificaParziale.class);
            CharSequence text3 = getString(R.string.SpaceInvaders);
            i.putExtra("nomegioco", text3);
            if(user==null){
                i.putExtra("nick", nickname);
            }
            startActivity(i);
        }
    }

    public void ClassificaPong (View view){
        if(!isNetworkConnected()){
            try {
                mDbHelper = new DBOpenHelper(this.getApplicationContext());
                mDB = mDbHelper.getReadableDatabase();
                Cursor c = mDB.rawQuery("SELECT * FROM " + DBOpenHelper.TABLE_PONG + " WHERE " + DBOpenHelper.USER_MAIL_PONG + " = '" + usernamemail + "'", null);
                c.moveToNext();
                pongwins = Integer.parseInt(c.getString(c.getColumnIndex(DBOpenHelper.NWIN)));
                pongloses = Integer.parseInt(c.getString(c.getColumnIndex(DBOpenHelper.NLOSE)));
            }catch(Exception e){gamescore=0;}
            Toast.makeText(getApplicationContext(),getResources().getQuantityString(R.plurals.numberOfVictories,pongwins,pongwins)+" "+getResources().getQuantityString(R.plurals.numberOfDefeats,pongloses,pongloses), Toast.LENGTH_SHORT).show();
        }else {
            Intent i = new Intent(Homepage.this, ClassificaParziale.class);
            CharSequence text4 = getString(R.string.Pong);
            i.putExtra("nomegioco", text4);
            if(user==null){
                i.putExtra("nick", nickname);
            }
            startActivity(i);
        }
    }


    @SuppressLint("RestrictedApi")
    public void Logged(){

        FirebaseFirestore firebaseFirestore = FirebaseFirestore.getInstance();
        CollectionReference utenti = firebaseFirestore.collection("utenti");

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        navigationView = findViewById(R.id.nav_view);

        View headerView = navigationView.getHeaderView(0);
        final TextView header = headerView.findViewById(R.id.textView12);

        if(user!=null) {
            utenti.whereEqualTo("email", usernamemail).get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                @Override
                public void onComplete(@NonNull Task<QuerySnapshot> task) {
                    if (task.isSuccessful()) {
                        for (QueryDocumentSnapshot document : task.getResult()) {
                            String headerst = document.getString("email");
                            header.setText(headerst);
                        }
                    }
                }
            });
        }else{
            String headers = usernamemail;
            header.setText(headers);
        }
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        //here is the main place where we need to work on.
        int id=item.getItemId();
        Intent i;
        switch (id){
            case R.id.nav_home:

                break;
            case R.id.nav_logout:
                mAuth.signOut();
                CharSequence text = getString(R.string.APresto);
                Toast.makeText(getApplicationContext(), text, Toast.LENGTH_SHORT).show();
                i = new Intent(this, sms1920.lisi_ludovico.salagiochi.MainActivity.class);
                startActivity(i);
                break;

            case R.id.nav_score:
                i = new Intent(this, CondivisionePunteggio.class);
                if(user==null) {
                    i.putExtra("nick", nickname);
                }
                startActivity(i);
                break;

            case R.id.nav_ranking:
                if(!isNetworkConnected()){
                    DBOpenHelper dbOpenHelper = new DBOpenHelper(this);
                    mDB = dbOpenHelper.getReadableDatabase();
                    try {
                        Cursor c = mDB.rawQuery("SELECT " + DBOpenHelper.SCORETETRIS + " FROM " + DBOpenHelper.TABLE_TETRIS + " WHERE " + DBOpenHelper.USER_MAIL_TETRIS + " = '" + usernamemail + "'", null);
                        c.moveToNext();
                        scoretetris = Integer.parseInt(c.getString(c.getColumnIndex(DBOpenHelper.SCORETETRIS)));
                    }catch(Exception e){}
                    try {
                        Cursor a = mDB.rawQuery("SELECT " + DBOpenHelper.SCOREBUBBLE + " FROM " + DBOpenHelper.TABLE_BUBBLE + " WHERE " + DBOpenHelper.USER_MAIL_BUBBLE + " = '" + usernamemail + "'", null);
                        a.moveToNext();
                        scorebubble = Integer.parseInt(a.getString(a.getColumnIndex(DBOpenHelper.SCOREBUBBLE)));
                    }catch(Exception e){}
                    try {
                        Cursor b = mDB.rawQuery("SELECT " + DBOpenHelper.SCORESNAKE + " FROM " + DBOpenHelper.TABLE_SNAKE + " WHERE " + DBOpenHelper.USER_MAIL_SNAKE + " = '" + usernamemail + "'", null);
                        b.moveToNext();
                        scoresnake = Integer.parseInt(b.getString(b.getColumnIndex(DBOpenHelper.SCORESNAKE)));
                    }catch(Exception e){}
                    try {
                        Cursor d = mDB.rawQuery("SELECT * FROM " + DBOpenHelper.TABLE_SPINV + " WHERE " + DBOpenHelper.USER_MAIL_SPINV + " = '" + usernamemail + "'", null);
                        d.moveToNext();
                        spinvlevel = Integer.parseInt(d.getString(d.getColumnIndex(DBOpenHelper.LEVELSPINV)));
                        scorespace = Integer.parseInt(d.getString(d.getColumnIndex(DBOpenHelper.SCORESPINV)));
                    }catch(Exception e){}
                    try {
                        Cursor e = mDB.rawQuery("SELECT * FROM " + DBOpenHelper.TABLE_PONG + " WHERE " + DBOpenHelper.USER_MAIL_PONG + " = '" + usernamemail + "'", null);
                        e.moveToNext();
                        pongloses = Integer.parseInt(e.getString(e.getColumnIndex(DBOpenHelper.NLOSE)));
                        pongwins = Integer.parseInt(e.getString(e.getColumnIndex(DBOpenHelper.NWIN)));
                        mDB.close();
                    }catch(Exception e){ }
                    punteggiototale=scoresnake+scoretetris+(scorebubble/10.0)+((scorespace/500.0)*spinvlevel);
                    if(pongwins+pongloses!=0){
                        punteggiototale+=+((pongwins*10.0)/pongwins+pongloses);
                    }
                    CharSequence text1 = getString(R.string.TuoPunteggio);
                    Toast.makeText(getBaseContext(), text1+" "+punteggiototale, Toast.LENGTH_SHORT).show();
                }else{
                    i = new Intent(this, ClassificaGenerale.class);
                    if (user == null) {
                        i.putExtra("nick", nickname);
                    }
                    startActivity(i);
                }
                break;
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private static long back_pressed;
    @Override
    public void onBackPressed(){
        if (back_pressed + 2000 > System.currentTimeMillis()){
            mAuth.signOut();
            super.onBackPressed();
        }
        else{
            CharSequence text = getString(R.string.UscitaApp);
            Toast.makeText(getBaseContext(), text, Toast.LENGTH_SHORT).show();
            back_pressed = System.currentTimeMillis();
        }
    }
    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) this.getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().isConnected();
    }
}