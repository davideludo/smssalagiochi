package sms1920.lisi_ludovico.salagiochi;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.lisi_ludovico.salagiochi.R;

public class MainActivity extends AppCompatActivity {

    FirebaseAuth auth;
    FirebaseUser userFirebase;
    FirebaseAuth mAuth = FirebaseAuth.getInstance();
    FirebaseUser user=mAuth.getCurrentUser();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        setContentView(R.layout.activity_main);

        auth=FirebaseAuth.getInstance();

        TextView Entra = findViewById(R.id.textView2);
        final EditText email = findViewById(R.id.editText);
        final EditText password = findViewById(R.id.editText2);

        if(user!=null){
            startActivity(new Intent(this,Homepage.class));
        }

        Entra.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view){
                String etemail = email.getText().toString();
                String etpassword = password.getText().toString();
                if (etemail.isEmpty()) {
                    CharSequence text = getString(R.string.InsertEmail);
                    Toast.makeText(getApplicationContext(), text, Toast.LENGTH_SHORT).show();
                    return;
                }
                if (etpassword.isEmpty()) {
                    CharSequence text1 = getString(R.string.InsertPassword);
                    Toast.makeText(getApplicationContext(), text1, Toast.LENGTH_SHORT).show();
                    return;
                }
                auth.signInWithEmailAndPassword(etemail, etpassword).addOnCompleteListener(MainActivity.this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            userFirebase = auth.getCurrentUser();
                            CharSequence text2 = getString(R.string.WelcomeBack);
                            Toast.makeText(MainActivity.this, text2, Toast.LENGTH_SHORT).show();
                            Intent i = new Intent(MainActivity.this, Homepage.class);
                            startActivity(i);
                        } else {
                            CharSequence text3 = getString(R.string.AuthenticationFailed);
                            Toast.makeText(MainActivity.this, text3, Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            }
        });
    }

    public void Registrati (View view){
        Intent i = new Intent(this, SignUp.class);
        startActivity(i);
    }

    private static long back_pressed;
    @Override
    public void onBackPressed(){
        if (back_pressed + 2000 > System.currentTimeMillis()){
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_HOME);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        }
        else{
            CharSequence text = getString(R.string.UscitaApp);
            Toast.makeText(getBaseContext(), text, Toast.LENGTH_SHORT).show();
            back_pressed = System.currentTimeMillis();
        }
    }

    public void AccessoOspite(View view){
        Intent i = new Intent(this, Ospite.class);
        startActivity(i);
    }
}
