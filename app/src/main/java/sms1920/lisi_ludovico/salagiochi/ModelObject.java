package sms1920.lisi_ludovico.salagiochi;

import com.lisi_ludovico.salagiochi.R;

public enum ModelObject {
    TETRIS(R.string.Tetris, R.layout.gioco1),
    PUZZLEBUBBLE(R.string.PuzzleBubble, R.layout.gioco2),
    SNAKE(R.string.Snake, R.layout.gioco3),
    PONG(R.string.Pong, R.layout.gioco4),
    SPACEINVADERS(R.string.SpaceInvaders, R.layout.gioco5);


    private int mTitleResId;
    private int mLayoutResId;

    ModelObject(int titleResId, int layoutResId) {
        mTitleResId = titleResId;
        mLayoutResId = layoutResId;
    }

    public int getTitleResId() {
        return mTitleResId;
    }

    public int getLayoutResId() {
        return mLayoutResId;
    }
}
