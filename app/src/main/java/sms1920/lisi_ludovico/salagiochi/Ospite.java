package sms1920.lisi_ludovico.salagiochi;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.lisi_ludovico.salagiochi.R;

public class Ospite extends AppCompatActivity {
    TextView accesso;
    EditText nick;
    String nickname;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ospite);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        accesso=findViewById(R.id.accessosp);
        nick=findViewById(R.id.editText);

        accesso.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                nickname=nick.getText().toString();
                if(nickname.isEmpty()){
                    CharSequence text1 = getString(R.string.InsertNick);
                    Toast.makeText(getApplicationContext(), text1, Toast.LENGTH_SHORT).show();
                }else{
                    Intent i = new Intent(Ospite.this, Homepage.class);
                    i.putExtra("nick", nickname);
                    startActivity(i);
                }
            }
        });
    }

    public void Accedi (View view){
        Intent i = new Intent(this, MainActivity.class);
        startActivity(i);
    }

    private static long back_pressed;
    @Override
    public void onBackPressed(){
        if (back_pressed + 2000 > System.currentTimeMillis()){
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_HOME);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        }
        else{
            CharSequence text = getString(R.string.UscitaApp);
            Toast.makeText(getBaseContext(), text, Toast.LENGTH_SHORT).show();
            back_pressed = System.currentTimeMillis();
        }
    }
}
