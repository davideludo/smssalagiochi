package sms1920.lisi_ludovico.salagiochi;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FirebaseFirestore;
import com.lisi_ludovico.salagiochi.R;

import java.util.HashMap;
import java.util.Map;

public class SignUp extends AppCompatActivity {
    FirebaseAuth auth;
    FirebaseUser userFirebase;
    FirebaseFirestore  db= FirebaseFirestore.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_sign_up);

        final EditText email = findViewById(R.id.editText);
        final EditText pass = findViewById(R.id.editText2);
        final EditText conf = findViewById(R.id.editText21);
        final TextView registra = findViewById(R.id.textView2);
        auth = FirebaseAuth.getInstance();

        registra.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String password = pass.getText().toString();
                String confpassword = conf.getText().toString();
                if (email.getText().toString().isEmpty()) {
                    CharSequence text = getString(R.string.InsertEmail);
                    Toast.makeText(getApplicationContext(), text, Toast.LENGTH_SHORT).show();
                    return;
                }
                if (pass.getText().toString().isEmpty()) {
                    CharSequence text1 = getString(R.string.InsertAPassword);
                    Toast.makeText(getApplicationContext(), text1, Toast.LENGTH_SHORT).show();
                    return;
                }
                if (conf.getText().toString().isEmpty()) {
                    CharSequence text1 = getString(R.string.InsertAPassword);
                    Toast.makeText(getApplicationContext(), text1, Toast.LENGTH_SHORT).show();
                    return;
                }
                if (password.equals(confpassword)) {
                    auth.createUserWithEmailAndPassword(email.getText().toString(), password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {

                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (task.isSuccessful()) {
                                Map<String, String> user = new HashMap<>();
                                user.put("email",email.getText().toString());
                                user.put("password",pass.getText().toString());
                                db.collection("utenti").add(user);
                            }
                        }
                    });
                }else{
                    CharSequence text = getString(R.string.NotMatchPass);
                    Toast.makeText(SignUp.this, text, Toast.LENGTH_LONG).show();

                }
                auth.signInWithEmailAndPassword(email.getText().toString(), password).addOnCompleteListener(SignUp.this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            userFirebase = auth.getCurrentUser();
                            CharSequence text1 = getString(R.string.Welcome);
                            Toast.makeText(SignUp.this, text1, Toast.LENGTH_SHORT).show();
                            Intent i = new Intent(SignUp.this, Homepage.class);
                            startActivity(i);
                        } else {
                            CharSequence text2 = getString(R.string.RegistrationFailed);
                            Toast.makeText(SignUp.this, text2, Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            }
        });
    }

    public void Accedi (View view){
        Intent i = new Intent(this, MainActivity.class);
        startActivity(i);
    }

    private static long back_pressed;
    @Override
    public void onBackPressed(){
        if (back_pressed + 2000 > System.currentTimeMillis()){
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_HOME);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        }
        else{
            CharSequence text = getString(R.string.UscitaApp);
            Toast.makeText(getBaseContext(), text, Toast.LENGTH_SHORT).show();
            back_pressed = System.currentTimeMillis();
        }
    }
}
