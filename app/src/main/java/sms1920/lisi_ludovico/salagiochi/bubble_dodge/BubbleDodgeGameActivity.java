package sms1920.lisi_ludovico.salagiochi.bubble_dodge;

import android.content.Context;
import android.content.pm.ActivityInfo;
import android.graphics.Point;
import android.os.Bundle;
import android.view.Display;

import androidx.appcompat.app.AppCompatActivity;

public class BubbleDodgeGameActivity extends AppCompatActivity {
    GameView gameView;
    private int value;
    String nickname;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        //creiamo un oggetto di tipo Display
        Display display= getWindowManager().getDefaultDisplay();
        //Otteniamo la risoluzione dello schermo come un oggetto point
        Point size= new Point();
        display.getSize(size);
        Bundle bundle= getIntent().getExtras();
        if (bundle != null)
            value= bundle.getInt("value");
            nickname=bundle.getString("nick");
        gameView = new GameView(this, size.x, size.y, value,nickname);
        //Inseriamo la gameview nel contentView
        setContentView(gameView);

    }

    @Override
    protected void onPause() {
        super.onPause();
        gameView.pause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        gameView.resume();
    }
}
