package sms1920.lisi_ludovico.salagiochi.bubble_dodge;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.SeekBar;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.lisi_ludovico.salagiochi.R;

public class BubbleDodgeMain extends Activity implements View.OnClickListener {
    private Button buttonPlay;
    private SeekBar mySeekBar;
    private int value;
    FirebaseAuth mAuth = FirebaseAuth.getInstance();
    FirebaseUser user=mAuth.getCurrentUser();
    String nickname;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        setContentView(R.layout.activity_bubble_dodge);
        if(user==null) {
            Intent i;
            i = getIntent();
            nickname = i.getStringExtra("nick");
        }

        buttonPlay= findViewById(R.id.buttonRiprova);
        mySeekBar= findViewById(R.id.seekBarSensore);
        //Listener
        buttonPlay.setOnClickListener(this);
        //Seekbar
        mySeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                value=progress;
            }
            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {}
            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {}
        });
    }

    @Override
    public void onClick(View v) {
        if(v==buttonPlay){
            Context context=getApplicationContext();
            Intent startGame= new Intent(this, BubbleDodgeGameActivity.class);
            //Passo il valore della seekbar all'ultimo rilevamento
            startGame.putExtra("value",value);
            startGame.putExtra("nick",nickname);
            startActivity(startGame);
        }
    }
}
