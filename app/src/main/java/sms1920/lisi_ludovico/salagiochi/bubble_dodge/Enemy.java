package sms1920.lisi_ludovico.salagiochi.bubble_dodge;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;

import com.lisi_ludovico.salagiochi.R;

import java.util.Random;

public class Enemy {
    private Bitmap bitmap;
    private int x;
    private int y;
    private int speed;

    private int maxX;
    private int minX;
    private int maxY;
    private int minY;

    private int directionX;
    private int directionY;

    private Rect detectCollision;

    public Enemy(Context context, int screenX, int screenY){
        bitmap= BitmapFactory.decodeResource(context.getResources(), R.drawable.enemy);
        maxX= screenX;
        maxY= screenY;
        minX= 0;
        minY= 0;
        //Valori random per velocità e direzione di x e y
        Random generator = new Random();
        speed= generator.nextInt(4)+2;

        directionX= generator.nextInt(4)+2;
        directionY= generator.nextInt(4)+2;

        //Spawn casuali in un area compresa tra min e max meno la dimensione dello sprite
        x= generator.nextInt(maxX-bitmap.getWidth());
        y= minY;
        detectCollision= new Rect(x, y, bitmap.getWidth(), bitmap.getHeight());
    }

    public void update(){
        x+=speed*directionX;
        y+=speed*directionY;
        bounce();
        detectCollision.left= x;
        detectCollision.top= y;
        detectCollision.right= x+ bitmap.getWidth()-50;
        detectCollision.bottom= y+ bitmap.getHeight()-50;

    }
    private void bounce() {
        if(x<minX||x>maxX-bitmap.getWidth()){
            directionY*=-1;
            speed*=-1;
        }
        if(y<minY||y>maxY-bitmap.getWidth()){
            directionX*=-1;
            speed*=-1;

        }
    }

    //getters
    public Rect getDetectCollision() {  return detectCollision; }
    public Bitmap getBitmap() { return bitmap;  }
    public int getX() { return x;   }
    public int getY() { return y;   }
}
