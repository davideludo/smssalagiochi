package sms1920.lisi_ludovico.salagiochi.bubble_dodge;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.lisi_ludovico.salagiochi.R;

public class GameOver extends Activity implements View.OnClickListener {

    private Button buttonHomepage;
    private Button buttonRiprova;
    private Integer score;
    String nickname;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        setContentView(R.layout.activity_game_over);
        Intent i;
        i = getIntent();
        nickname=i.getStringExtra("nick");
        buttonHomepage= findViewById(R.id.buttonHomepage);
        buttonRiprova= findViewById(R.id.buttonRiprova);
        TextView textViewPunteggio = findViewById(R.id.textViewPunteggio);
        buttonHomepage.setOnClickListener(this);
        buttonRiprova.setOnClickListener(this);
        Bundle bundle= getIntent().getExtras();
        if (bundle != null)
            score= bundle.getInt("score");
        textViewPunteggio.append(score.toString());
    }

    @Override
    public void onClick(View v) {
         if(v== buttonHomepage){
            Intent homepage= new Intent(this, BubbleDodgeMain.class);
            startActivity(homepage);

        }else if(v== buttonRiprova){
            Intent riprova= new Intent(this, BubbleDodgeGameActivity.class);
            riprova.putExtra("nick", nickname);
            startActivity(riprova);
        }
    }
}
