package sms1920.lisi_ludovico.salagiochi.bubble_dodge;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import androidx.annotation.NonNull;
import androidx.core.content.res.ResourcesCompat;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.firestore.SetOptions;
import com.lisi_ludovico.salagiochi.R;

import java.util.HashMap;
import java.util.Map;

import sms1920.lisi_ludovico.salagiochi.DBOpenHelper;

public class GameView extends SurfaceView implements Runnable {
    FirebaseAuth mAuth = FirebaseAuth.getInstance();
    FirebaseUser user=mAuth.getCurrentUser();
    protected boolean playing;
    String usernamemail;
    Context context;
    private Thread gameThread = null;
    private Player player;
    private Paint paint;
    private Canvas canvas;
    private SurfaceHolder surfaceHolder;
    String nickname;
    private Enemy[] enemies;
    private int extraEnemy;
    private int maxEnemy;

    private int life;
    private boolean[] lifes;
    private boolean isGameOver;
    private int enemyIndex;

    private int score;
    private int oldScore;
    private int bestScore;
    private SQLiteDatabase mDB = null;
    private DBOpenHelper mDbHelper;
    private Bitmap backgroundInGame;
    private Bitmap background;
    private Bitmap[] playerLife;
    FirebaseFirestore db=FirebaseFirestore.getInstance();
    //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
    private Typeface typeface;
    public GameView(Context context, int screenX, int screenY, int value,String nickname) {
        super(context);
        this.context= context;
        //Default State
        isGameOver=false;
        enemyIndex=-1;
        this.nickname=nickname;
        //nuovo player
        player= new Player(context, screenX, screenY, value);
        life =3;
        lifes= new boolean[life];
        playerLife= new Bitmap[life];
        for(int i=0;i<life; i++){
            playerLife[i]= BitmapFactory.decodeResource(context.getResources(),R.drawable.alien_life);
            lifes[i]=true;
        }
        //La prima vita avrà 2 come indice essendo un array di 3 elementi, senza questa assegnazione andrei in indexOfBoundException
        life= 2;
        //inizializiamo gli strumenti per disegnare
        surfaceHolder= getHolder();
        paint= new Paint();
        //nemici 2 saranno i nemici iniziali, poi saliranno fino a max
        extraEnemy= 2;
        maxEnemy= 10;
        enemies= new Enemy[maxEnemy];
        for(int i= 0; i<maxEnemy; i++)
            enemies[i]= new Enemy(context, screenX, screenY);
        //punteggio
        score= 0;
        oldScore= score;
        //bitmap
        background = BitmapFactory.decodeResource(context.getResources(),R.drawable.background_bd);
        backgroundInGame = BitmapFactory.decodeResource(context.getResources(),R.drawable.background_ingame_bd);
        //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
        typeface = ResourcesCompat.getFont(context, R.font.customfont);
    }

    @Override
    public void run() {
        while (playing) {
            update();
            draw();
            extraEnemy();
            control();
        }
    }

    private void update() {
        //Avvio il counter del punteggio
        score++;
        //Update player
        player.update();
        //Update enemy
        for(int i= 0; i<extraEnemy; i++){
            enemies[i].update();
            if(Rect.intersects(player.getDetectCollision(),enemies[i].getDetectCollision())) {
                if(life>=0){
                    if(lifes[life]) {
                        if (enemyIndex == -1) {
                            Log.d("HIT", "COLPITO ");
                            Log.d("HIT", "PRIMO HIT LIFE : " + life);
                            lifes[life] = false;
                            life -= 1;
                            enemyIndex = i;
                        } else if (enemyIndex != i) {
                            Log.d("HIT", "COLPITO ");
                            Log.d("HIT", "MI HA COLPITO UN ALTRO NEMICO LIFE : " + life);
                            lifes[life] = false;
                            life -= 1;
                            enemyIndex = i;
                        }
                    }
                }else {
                    if(user==null) {
                        usernamemail=nickname;
                    }else{
                        usernamemail=user.getEmail();
                    }
                    try {
                        mDbHelper = new DBOpenHelper(this.getContext());
                        mDB = mDbHelper.getReadableDatabase();
                        Cursor c = mDB.rawQuery("SELECT " + DBOpenHelper.SCOREBUBBLE + " FROM " + DBOpenHelper.TABLE_BUBBLE + " WHERE " + DBOpenHelper.USER_MAIL_BUBBLE + " = '"+usernamemail+"'", null);
                        c.moveToNext();

                        bestScore = Integer.parseInt(c.getString(c.getColumnIndex(DBOpenHelper.SCOREBUBBLE)));
                        if(score>bestScore) {

                            ContentValues cv = new ContentValues();
                            cv.put(DBOpenHelper.SCOREBUBBLE,score);
                            mDB.update(DBOpenHelper.TABLE_BUBBLE,cv,DBOpenHelper.USER_MAIL_BUBBLE+"= '"+usernamemail+"'",null);
                        }
                        mDB.close();
                    }catch(Exception e){
                        mDbHelper = new DBOpenHelper(this.getContext());
                        // writing on DB
                        mDB = mDbHelper.getWritableDatabase();
                        ContentValues values = new ContentValues();
                        values.put(DBOpenHelper.USER_MAIL_BUBBLE, usernamemail);
                        values.put(DBOpenHelper.SCOREBUBBLE, score);
                        mDB.insert(DBOpenHelper.TABLE_BUBBLE, null, values);
                        mDB.close();
                    }

                    if(isNetworkConnected()) {
                        //firebase add
                        db.collection("utenti").whereEqualTo("email", usernamemail).get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                            @Override
                            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                                if (task.isSuccessful()) {
                                    for (QueryDocumentSnapshot document : task.getResult()) {
                                        try {//best_onscore è il miglior score online

                                            int best_onscore = document.getLong("bubble_score").intValue();
                                            if (score > best_onscore) {
                                                Map<String, Object> data = new HashMap<>();
                                                data.put("bubble_score", score);
                                                db.collection("utenti").document(document.getId()).set(data, SetOptions.merge());
                                            }
                                        } catch (Exception e) {
                                            Map<String, Object> data = new HashMap<>();
                                            data.put("bubble_score", score);
                                            db.collection("utenti").document(document.getId()).set(data, SetOptions.merge());
                                        }
                                    }
                                }
                            }
                        });
                    }
                    isGameOver= true;
                    playing= false;
                }
            }
        }
    }

    private void draw() {
        if(surfaceHolder.getSurface().isValid()){
            //blocca il canvas
            canvas= surfaceHolder.lockCanvas();
            //background
            canvas.drawBitmap(backgroundInGame,0,0,paint);
            //punteggio
            paint.setTextSize(50);
            //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
            paint.setTypeface(typeface);
            paint.setColor(Color.rgb(142,255,188));
            canvas.drawText("Punteggio:"+score,100,80,paint);
            //player
            player.draw(canvas);
            //nemici
            for(int i= 0; i<extraEnemy; i++)
                canvas.drawBitmap(enemies[i].getBitmap(), enemies[i].getX(), enemies[i].getY(), paint);
            //vite
            if(lifes[0])canvas.drawBitmap(playerLife[0],canvas.getWidth()/2,25,paint);
            if(lifes[1])canvas.drawBitmap(playerLife[0],canvas.getWidth()/2+100,25,paint);
            if(lifes[2])canvas.drawBitmap(playerLife[0],canvas.getWidth()/2+200,25,paint);
            //gameover
            if(isGameOver){
                paint.setTextSize(150);
                paint.setColor(Color.rgb(142,255,188));
                paint.setTextAlign(Paint.Align.CENTER);
                //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                paint.setTypeface(typeface);
                canvas.drawBitmap(background,-5,-5,paint);
                canvas.drawText("GAME OVER",canvas.getWidth()/2,canvas.getHeight()/2,paint);
                paint.setTextSize(80);
                canvas.drawText("TAP TO CONTINUE...",canvas.getWidth()-500,canvas.getHeight()-50,paint);
            }
            //sblocca il canvas
            surfaceHolder.unlockCanvasAndPost(canvas);
        }
    }

    private void extraEnemy(){
        if(score==oldScore+100){
            Log.d("EX_ENEMY","INGRESSO NELL'AREA SCORE :"+score+" OLDSCORE : "+oldScore);
            switch (score){
                case 100: case 200: case 300: case 400: case 500: case 600: case 700: case 800:
                    extraEnemy++;
                    oldScore= score;
                    break;
            }
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if ((event.getAction())== MotionEvent.ACTION_DOWN) {
            player.setJump(true);
        }
        if(isGameOver){
            if(event.getAction()==MotionEvent.ACTION_DOWN){
                Intent gameOver= new Intent(context,GameOver.class);
                gameOver.putExtra("score",score);
                gameOver.putExtra("nick",nickname);
                context.startActivity(gameOver);
            }
        }
        return true;
    }

    private void control() {
        try {
            gameThread.sleep(17);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
    public void pause() {
        //when the game is paused
        //setting the variable to false
        playing = false;
        try {
            //stopping the thread
            gameThread.join();
        } catch (InterruptedException e) {e.printStackTrace();}
    }
    public void resume() {
        //when the game is resumed
        //starting the thread again
        playing = true;
        gameThread = new Thread(this);
        gameThread.start();
    }
    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().isConnected();
    }
}
