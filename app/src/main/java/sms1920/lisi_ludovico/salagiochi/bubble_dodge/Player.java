package sms1920.lisi_ludovico.salagiochi.bubble_dodge;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.util.Log;

import com.lisi_ludovico.salagiochi.R;

public class Player implements SensorEventListener {
    //coordinates
    private int x;
    private int y;
    //sensor
    SensorManager sensorManager;
    private int sensorX;
    private int sensorCustomValue;
    //jump
    private boolean jump;
    //screen bound
    private int minY,maxY;
    private int minX,maxX;

    private Rect detectCollision;

    //animation
    private Bitmap idleState;
    private Animation idle;
    private Animation walkRight;
    private Animation walkLeft;
    private AnimationManager animationManager;

    public Player(Context context, int screenX, int screenY, int value){
        idleState= BitmapFactory.decodeResource(context.getResources(),R.drawable.aliengreen);
        x= screenX/2;
        y= screenY;
        maxY= screenY - idleState.getHeight();
        maxX= screenX - idleState.getWidth();
        minX= 0;
        minY= 0;
        //Sensore
        sensorCustomValue= value;
        sensorManager= (SensorManager)
                context.getSystemService(Context.SENSOR_SERVICE);
        if (sensorManager != null) {
            sensorManager.registerListener((SensorEventListener) this,
                    sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER),
                    SensorManager.SENSOR_DELAY_GAME);
        }

        detectCollision= new Rect(x, y, idleState.getWidth(),idleState.getHeight());

        Bitmap walkR1= BitmapFactory.decodeResource(context.getResources(), R.drawable.aliengreen_walk1);
        Bitmap walkR2= BitmapFactory.decodeResource(context.getResources(),R.drawable.aliengreen_walk2);
        Bitmap walkL1= BitmapFactory.decodeResource(context.getResources(),R.drawable.aliengreen_walk1_left);
        Bitmap walkL2= BitmapFactory.decodeResource(context.getResources(),R.drawable.aliengreen_walk2_left);

        idle= new Animation(new Bitmap[]{idleState},2);
        walkRight= new Animation(new Bitmap[]{walkR1,walkR2},0.5f);
        walkLeft= new Animation(new Bitmap[]{walkL1,walkL2},0.5f);

        animationManager= new AnimationManager(new Animation[]{idle,walkRight,walkLeft});

    }

    public void draw(Canvas canvas){
        animationManager.draw(canvas,detectCollision);
    }

    public void update(){
        float oldLeft= detectCollision.left;
        int state=0;
        x+=sensorX;
        if(jump){
            y-=30;
            if(y<=25){
                jump=false;
            }
        }else y+=40;
        //Bounds
        if(x<=minX) x= minX;
        if(x>=maxX) x= maxX;
        if(y<=minY) y= minY;
        if(y>=maxY) y= maxY;

        detectCollision.left = x;
        detectCollision.top = y;
        detectCollision.right = x + idleState.getWidth()-50;
        detectCollision.bottom = y + idleState.getHeight()-50;

        //Modifichiamo lo stato dell'animazione in base alla posizione del rect collegato al bitmap
        if(detectCollision.left-oldLeft>2) state= 1;
        else if(detectCollision.left-oldLeft<-2) state= 2;

        animationManager.playAnim(state);
        animationManager.update();
    }

    @Override
    public void onSensorChanged(SensorEvent event){
        sensorX= (int)event.values[1]*(8+sensorCustomValue);
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {    }

    //Setter
    public void setJump(boolean jump) {
        this.jump = jump;
    }
    //Getters
    public Rect getDetectCollision() {  return detectCollision; }
    public int getX() { return x;   }
    public int getY() { return y;   }
}
