package sms1920.lisi_ludovico.salagiochi.pong;

public class PongOb {
    Integer nwins,nloses;

    public void PongOb(){
    }
    public int getNwins(){
        return nwins;
    }
    public int getNloses(){
        return nloses;
    }
    public void setNloses(Integer nloses){
        this.nloses=nloses;
    }
    public void setNwins(Integer nwins){
        this.nwins=nwins;
    }

    public boolean isNull(){
        return nwins==0 && nloses==0;
    }

    public int compareTo(PongOb value) {
        if(value.nwins==nwins&&value.nloses==nloses)return 0;
        if(value.nwins/(value.nwins+value.nloses)>nwins/(nwins+nloses))return 1;
        return -1;
    }
}
