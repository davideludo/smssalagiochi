package sms1920.lisi_ludovico.salagiochi.space_invaders;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.text.InputType;
import android.util.Log;
import android.widget.EditText;

import com.lisi_ludovico.salagiochi.R;

public class BestScoreScreen {
    private GameView gameView;
    private Context context;
    private boolean bestScore = false;
    private Paint titlePaint = new Paint();
    private Paint descriptionPaint = new Paint();
    private Typeface typeface = Typeface.create("Helvetica",Typeface.BOLD);
    String nameBest1,nameBest2,nameBest3;
    int scoreBest1, scoreBest2,scoreBest3;
    private int x;
    private int y;
    private String playerName;
    AlertDialog.Builder builder;
    private int actualScore;
    private boolean scoreSet = false;


    public BestScoreScreen(GameView gameView, Context context){
        this.context = context;
        this.gameView = gameView;

        //Get reference to preferences file

        titlePaint.setTypeface(typeface);
        titlePaint.setColor(Color.WHITE);
        titlePaint.setTextSize(60);
        titlePaint.setTextAlign(Paint.Align.CENTER);
        descriptionPaint.setColor(Color.WHITE);
        descriptionPaint.setTextAlign(Paint.Align.CENTER);
        descriptionPaint.setTextSize(35);
        x = gameView.getWidth()/2;
    }

    @SuppressLint("WrongCall")
    public void draw(Canvas canvas) {
        y = gameView.getHeight()/2;
        canvas.drawText("Game Over!   Score:"+actualScore,x,y,titlePaint);
        canvas.drawText("Tap to play!",x,y+50,titlePaint);
    }

    public void showInput(int actualScore) {
        this.actualScore = actualScore;
    }
}