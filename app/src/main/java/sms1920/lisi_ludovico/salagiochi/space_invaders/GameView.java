package sms1920.lisi_ludovico.salagiochi.space_invaders;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.SoundPool;
import android.net.ConnectivityManager;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import androidx.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.firestore.SetOptions;
import com.lisi_ludovico.salagiochi.R;

import java.util.HashMap;
import java.util.Map;

import sms1920.lisi_ludovico.salagiochi.DBOpenHelper;

public class GameView extends SurfaceView {
    FirebaseFirestore db=FirebaseFirestore.getInstance();
    FirebaseAuth mAuth = FirebaseAuth.getInstance();
    FirebaseUser user=mAuth.getCurrentUser();
    private MainActivity activity;
    String nickname;
    private Context context;
    private GameLoopThread gameLoopThread;
    private InvaderSpaceFleet invaderSpaceFleet;
    private boolean firstStart = true;
    private Explosion explosion;
    private GoodSpaceShip goodSpaceShip;
    private Shoot goodSpaceShipShoot;
    private Shoot invaderSpaceShipShoot;
    private Shield shield;
    private long lastClick;
    private Bitmap bmpExplosion;
    private Bitmap bmpGoodShoot;
    private Bitmap bmpInvaderShoot;
    private Bitmap bmpBrick;
    private int level = 1;
    private int scoreValue=0;
    private SQLiteDatabase mDB = null;
    private DBOpenHelper mDbHelper;
    int bestScore,bestLevel;
    Paint scorePaint = new Paint();
    Typeface scoreTypeFace = Typeface.create("Helvetica",Typeface.BOLD);
    private int gameState = 1; // 1 WelcomeScreen; 2 Play; 3 GameOVer; 4 Next Level

    //WelcomeScreen welcomeScreen;
    LevelTransitionScreen levelTransitionScreen;
    BestScoreScreen scoreScreen;

    //Sound stuff
    private SoundPool soundPool;
    private MediaPlayer mPlayer;
    private int soundId;
    String usernamemail;

    public GameView(Context context, MainActivity activity) {
        super(context);
        this.activity = activity;
        this.context = context;

        // Config background Music
        if (mPlayer == null){
            mPlayer = MediaPlayer.create(getContext(), R.raw.music);
            mPlayer.setLooping(true);
        }

        // Load sounds
        soundPool = new SoundPool(5, AudioManager.STREAM_MUSIC, 0);
        soundId = soundPool.load(getContext(), R.raw.puaj, 1);

        gameLoopThread = new GameLoopThread(GameView.this);

        getHolder().addCallback(new SurfaceHolder.Callback() {

            @Override
            public void surfaceDestroyed(SurfaceHolder holder) {
                boolean retry = true;
                gameLoopThread.setRunning(false);
                while (retry) {
                    try {
                        gameLoopThread.join();
                        retry = false;
                    } catch (InterruptedException e) {}
                }
            }

            @Override
            public void surfaceCreated(SurfaceHolder holder) {
                bmpExplosion = BitmapFactory.decodeResource(getResources(), R.drawable.explosion);
                bmpGoodShoot = BitmapFactory.decodeResource(getResources(), R.drawable.goodspaceshipshoot);
                bmpInvaderShoot = BitmapFactory.decodeResource(getResources(), R.drawable.invaderspaceshipshoot);
                bmpBrick = BitmapFactory.decodeResource(getResources(), R.drawable.brick);
                createScreens();
                createGoodSpaceShip();
                createInvaderSpaceFleet(level);
                createGoodSpaceShipShoot(false);
                createInvaderSpaceShipShoot(false);
                createExplosion(false,false);
                createShield();

                gameLoopThread.setRunning(true);
                if(firstStart){
                    gameLoopThread.start();
                    firstStart=false;
                }
            }

            @Override
            public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {}
        });
    }

    public GameLoopThread getGameLoopThread() {
        return gameLoopThread;
    }

    private void createScreens(){
        //this.welcomeScreen = new WelcomeScreen(this);
        this.levelTransitionScreen = new LevelTransitionScreen(this);
        this.scoreScreen = new BestScoreScreen(this,context); }

    // Creates a good ship
    private void createGoodSpaceShip(){
        Bitmap bmp = BitmapFactory.decodeResource(getResources(), R.drawable.goodspaceship);
        this.goodSpaceShip = new GoodSpaceShip(this,bmp,1,13,context);
    }

    // Creates an invader ship
    private void createInvaderSpaceFleet(int level){
        int landingHeight = goodSpaceShip.getHeight();
        this.invaderSpaceFleet = new InvaderSpaceFleet(this,this.getHeight()-landingHeight,level);
    }

    //Creates a good ship shoot
    private void createGoodSpaceShipShoot(boolean alive) {
        int spriteColumns = 2;
        int spriteRows = 1;
        int x = goodSpaceShip.getX()+goodSpaceShip.getWidth()/2 - (bmpGoodShoot.getWidth()/spriteColumns) / 2;
        int y = goodSpaceShip.getY();
        int xSpeed = goodSpaceShip.getXSpeed();
        this.goodSpaceShipShoot = new Shoot(this,bmpGoodShoot,spriteRows,spriteColumns,x,y,xSpeed,false,alive);
    }

    //Creates a invader ship shoot
    private void createInvaderSpaceShipShoot(boolean alive) {
        int spriteColumns = 9;
        int spriteRows = 1;
        Sprite shooterShip = invaderSpaceFleet.getShooter();
        int x = shooterShip.getX()+shooterShip.getWidth()/2 - (bmpInvaderShoot.getWidth()/spriteColumns) / 2;
        int y = shooterShip.getY();
        this.invaderSpaceShipShoot = new Shoot(this,bmpInvaderShoot,spriteRows,spriteColumns,x,y,0,true,alive);
    }

    //Creates a explosion
    private void createExplosion(boolean alive, boolean invaderShip) {
        int spriteColumns = 25;
        int spriteRows = 1;
        int x,y;
        if(invaderShip){
            x = goodSpaceShipShoot.getX()+goodSpaceShipShoot.getWidth()/2 - (bmpExplosion.getWidth()/spriteColumns) / 2;
            y = goodSpaceShipShoot.getY()-goodSpaceShipShoot.getHeight()/2;
        }else{
            x= goodSpaceShip.getX();
            y= goodSpaceShip.getY();
        }
        explosion = new Explosion(this,bmpExplosion,spriteRows,spriteColumns,x,y,alive);
    }

    private void createShield(){
        int horizontalNumOfBrick = 8;
        int verticalNumOfBrick = 4;
        int spriteColumns = 4;
        int numberOfWalls = 3;

        int wallWidth = bmpBrick.getWidth()/spriteColumns * horizontalNumOfBrick;
        int separation = (this.getWidth() - wallWidth)/(numberOfWalls-1);

        int x_ini = 0;
        int y_ini = goodSpaceShip.getY() - bmpBrick.getWidth() - 10;

        for(int i = 0; i<numberOfWalls;i++){
            shield = new Shield(this,bmpBrick,spriteColumns,horizontalNumOfBrick,verticalNumOfBrick,x_ini,y_ini,numberOfWalls,separation);
        }
    }

    private void drawScore(Canvas canvas){
        scorePaint.setTypeface(scoreTypeFace);
        scorePaint.setColor(Color.WHITE);
        scorePaint.setTextSize(50);
        canvas.drawText("Score: "+scoreValue+"| Level: "+level,0,40,scorePaint);
    }

    private void checkCollisions() {
        //Collision good ship shoot vs invader fleet
        if (goodSpaceShipShoot.isAlive()){

            if(invaderSpaceFleet.isCollision(this.goodSpaceShipShoot)){
                scoreValue = scoreValue + invaderSpaceFleet.getPoints();
                goodSpaceShipShoot.setAlive(false);
                createExplosion(true,true);
            }
        }

        //Collision good ship shoot vs invader ship shoot
        if(goodSpaceShipShoot.isAlive() && invaderSpaceShipShoot.isAlive()){
            if(invaderSpaceShipShoot.isCollition(this.goodSpaceShipShoot)){
                this.invaderSpaceShipShoot.setAlive(false);
                this.goodSpaceShipShoot.setAlive(false);
            }
        }

        //Collision good ship       vs invader ship shoot
        if(invaderSpaceShipShoot.getBottom()>goodSpaceShip.getY() && invaderSpaceShipShoot.isAlive()){
            if(goodSpaceShip.isCollition(this.invaderSpaceShipShoot)){
                this.invaderSpaceShipShoot.setAlive(false);
                this.goodSpaceShip.setAlive(false);
                createExplosion(true,false);
            }
        }

        //Shield collisions
        if(shield.isAlive()){

            //Collision shield       vs invader ship shoot
            if(invaderSpaceShipShoot.getBottom()>shield.getY() && invaderSpaceShipShoot.isAlive()){
                if(shield.isCollision(invaderSpaceShipShoot)){
                    this.invaderSpaceShipShoot.setAlive(false);
                }
            }

            //Collision shield       vs good ship shoot
            if(goodSpaceShipShoot.isAlive()){
                if(shield.isCollision(goodSpaceShipShoot)){
                    this.goodSpaceShipShoot.setAlive(false);
                }
            }

            //Collision shield       vs invader fleet
            if(shield.isCollision2(invaderSpaceFleet)){

            }
        }
    }
        public void updateScore(){
            if(user==null) {
                Intent i;
                i =((Activity) context).getIntent();
                nickname = i.getStringExtra("nick");
                usernamemail=nickname;
            }else{
                usernamemail=user.getEmail();
            }
                try {
                    mDbHelper = new DBOpenHelper(this.getContext());
                    mDB = mDbHelper.getReadableDatabase();
                    Cursor c = mDB.rawQuery("SELECT * FROM " + DBOpenHelper.TABLE_SPINV + " WHERE " + DBOpenHelper.USER_MAIL_SPINV + " = '"+usernamemail+"'", null);
                    c.moveToNext();
                    bestLevel=Integer.parseInt(c.getString(c.getColumnIndex(DBOpenHelper.LEVELSPINV)));
                    if(level>bestLevel){
                        ContentValues cv = new ContentValues();
                        cv.put(DBOpenHelper.LEVELSPINV,level);
                        cv.put(DBOpenHelper.SCORESPINV,scoreValue);
                        mDB.update(DBOpenHelper.TABLE_SPINV,cv,DBOpenHelper.USER_MAIL_SPINV+"= '"+usernamemail+"'",null);
                    }else{

                    }
                    bestScore = Integer.parseInt(c.getString(c.getColumnIndex(DBOpenHelper.SCORESPINV)));
                    if(scoreValue>bestScore&&level==bestLevel) {

                        ContentValues cv = new ContentValues();
                        cv.put(DBOpenHelper.SCORESPINV,scoreValue);
                        mDB.update(DBOpenHelper.TABLE_SPINV,cv,DBOpenHelper.USER_MAIL_SPINV+"= '"+usernamemail+"'",null);
                    }
                    mDB.close();
                }catch(Exception e){
                    mDbHelper = new DBOpenHelper(this.getContext());
                    // writing on DB
                    mDB = mDbHelper.getWritableDatabase();
                    ContentValues values = new ContentValues();
                    values.put(DBOpenHelper.USER_MAIL_SPINV, usernamemail);
                    values.put(DBOpenHelper.LEVELSPINV, level);
                    values.put(DBOpenHelper.SCORESPINV, scoreValue);
                    mDB.insert(DBOpenHelper.TABLE_SPINV, null, values);
                    mDB.close();
                }

            if(isNetworkConnected()) {
                //firebase add
                db.collection("utenti").whereEqualTo("email", usernamemail).get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                try {//best_onscore è il miglior score online
                                    //best_onlevel è il miglior livello raggiunto online
                                    int best_onscore = document.getLong("spinv_score").intValue();
                                    int best_onlevel=document.getLong("spinv_level").intValue();
                                    if(level==best_onlevel) {


                                        if (scoreValue > best_onscore) {
                                            Map<String, Object> data = new HashMap<>();
                                            data.put("spinv_score", scoreValue);
                                            db.collection("utenti").document(document.getId()).set(data, SetOptions.merge());
                                        }
                                    }else{
                                        if(level>best_onlevel){
                                            Map<String, Object> data = new HashMap<>();
                                            data.put("spinv_score", scoreValue);
                                            data.put("spinv_level",level);
                                            db.collection("utenti").document(document.getId()).set(data, SetOptions.merge());
                                        }
                                    }
                                } catch (Exception e) {
                                    Map<String, Object> data = new HashMap<>();
                                    data.put("spinv_score", scoreValue);
                                    data.put("spinv_level",level);
                                    db.collection("utenti").document(document.getId()).set(data, SetOptions.merge());
                                }
                            }
                        } else {
                        }
                    }
                });
            }

            }
    public void drawGameScreen(Canvas canvas){
        if(this.goodSpaceShip.isAlive()){
            this.goodSpaceShip.onDraw(canvas);
        }

        this.invaderSpaceFleet.onDraw(canvas);

        if (this.goodSpaceShipShoot.isAlive()){
            this.goodSpaceShipShoot.onDraw(canvas);
        }

        if (this.invaderSpaceShipShoot.isAlive()){
            this.invaderSpaceShipShoot.onDraw(canvas);
        }else{
            if( Math.random()<0.1 ){
                createInvaderSpaceShipShoot(true);
            }
        }

        if(shield.isAlive()){
            shield.onDraw(canvas);
        }

        drawScore(canvas);

        checkCollisions();

        if(explosion.isAlive()){
            explosion.onDraw(canvas);
        }

        //If an invader arrives or if the ship is destroyed... Game Over
        if(invaderSpaceFleet.invaderArrive() || !goodSpaceShip.isAlive()){
            updateScore();
            gameState=3;
        }

        //If all invaders destroyed... NextLevel
        if(invaderSpaceFleet.allInvadersDestroyed()){
            level = level + 1;
            gameState=4;
        }
    }

    public void drawBestScoresScreen(Canvas canvas) {}

    @SuppressLint("WrongCall")
    @Override
    protected void onDraw(Canvas canvas) {
        canvas.drawColor(Color.BLACK);

        switch (gameState){
            case 1: //Welcome Screen
                gameState=2;
                break;
            case 2: //Play game
                drawGameScreen(canvas);
                break;
            case 3: //Game Over, (if best score -> enter name)

             scoreScreen.showInput(scoreValue);
             gameState=5;
                break;
            case 4: //NextLevel
                levelTransitionScreen.draw(canvas,level);
                break;
            case 5://Scores
                scoreScreen.draw(canvas);
                break;
            default:
                gameState = 1;
                break;
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if ((System.currentTimeMillis() - lastClick > 300) ) {
            lastClick = System.currentTimeMillis();

            synchronized (getHolder()) {

                switch(gameState){

                    case 1://Welcome Screen
                        gameState = 2;
                        break;
                    case 2:// Play game
                        if(!goodSpaceShipShoot.isAlive()){
                            createGoodSpaceShipShoot(true);
                            soundPool.play(soundId, 1, 1, 0, 0, 1);
                        }
                        break;
                    case 3://Enter name
                        gameState=5;
                        break;
                    case 4: //Next Level transition
                        gameState = 2;
                        createGoodSpaceShip();
                        createInvaderSpaceFleet(level);
                        createShield();
                        createGoodSpaceShipShoot(false);
                        createInvaderSpaceShipShoot(false);
                        break;
                    case 5:// Scores
                        gameState = 1;
                        level = 1;
                        scoreValue = 0;
                        createScreens();
                        createGoodSpaceShip();
                        createInvaderSpaceFleet(level);
                        createShield();
                        createGoodSpaceShipShoot(false);
                        createInvaderSpaceShipShoot(false);
                        break;
                    default:
                        gameState = 1;
                        break;
                }
            }
        }
        return true;
    }

    // Music functions
    public void startMusicPlayer(){
        mPlayer.start();
    }

    public void pauseMusicPlayer(){
        mPlayer.pause();
    }

    public void releaseMusicPlayer(){
        mPlayer.release();
        soundPool.release();
    }

    public void stopMusicPlayer(){
        mPlayer.stop();
    }
    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().isConnected();
    }
}
