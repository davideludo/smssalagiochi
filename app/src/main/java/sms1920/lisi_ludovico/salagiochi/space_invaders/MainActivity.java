package sms1920.lisi_ludovico.salagiochi.space_invaders;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Window;

public class MainActivity extends Activity {
    private GameView gameView;
    String nickname;
    Boolean ospite;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        gameView = new GameView(this,this);
        setContentView(gameView);
/*
        if(user==null) {
            Intent i;
            i = getIntent();
            nickname = i.getStringExtra("nick");
            ospite = i.getBooleanExtra("ospite", true);
        }

 */
    }

    @Override
    public void onPause(){
        super.onPause();
        gameView.pauseMusicPlayer();
    }

    @Override
    public void onResume(){
        super.onResume();
        gameView.startMusicPlayer();
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        gameView.stopMusicPlayer();
        gameView.releaseMusicPlayer();
    }
}
