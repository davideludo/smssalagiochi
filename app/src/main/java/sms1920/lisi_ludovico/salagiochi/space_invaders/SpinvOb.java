package sms1920.lisi_ludovico.salagiochi.space_invaders;

public class SpinvOb {
    private int score;
    private int level;

    public SpinvOb(int score, int level) {
        this.score=score;
        this.level=level;
    }

    public int getScore(){
        return score;
    }
    public int getLevel(){
    return level;
    }

    public int compareTo(SpinvOb value) {
        if(value.getLevel()==level&&value.getScore()==score) return 0;
        if(value.getLevel()>level||(value.getLevel()==level&&value.getScore()>score))return -1;
        return 1;
    }
}
