package sms1920.lisi_ludovico.salagiochi.space_invaders;

import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;

import com.lisi_ludovico.salagiochi.R;

public class WelcomeScreen {
    private GameView gameView;
    private Paint welcomeMessage = new Paint();
    private Paint detailsMessage = new Paint();

    private Typeface typeface = Typeface.create("Helvetica",Typeface.BOLD);
    private int x;
    private int y;
    private InvaderSpaceShip ship;

    public WelcomeScreen(GameView gameView){
        Bitmap bmpInvader_1 = BitmapFactory.decodeResource(gameView.getResources(), R.drawable.biginvader_1);
        this.gameView = gameView;
        welcomeMessage.setTextAlign(Paint.Align.CENTER);
        this.ship = new InvaderSpaceShip(gameView,
                bmpInvader_1,1,15,
                gameView.getWidth()/2-bmpInvader_1.getWidth()/30,
                gameView.getHeight()/2-bmpInvader_1.getHeight(),
                100);

        welcomeMessage.setTypeface(typeface);
        welcomeMessage.setColor(Color.WHITE);
        welcomeMessage.setTextSize(60);
        welcomeMessage.setTextAlign(Paint.Align.CENTER);
        x = gameView.getWidth()/2;
        y = gameView.getHeight()/6;

        detailsMessage.setColor(Color.LTGRAY);
        detailsMessage.setTextAlign(Paint.Align.CENTER);
        detailsMessage.setTextSize(30);

    }

    @SuppressLint("WrongCall")
    public static void draw(Canvas canvas) {
        Paint paint=new Paint();
        paint.setColor(Color.WHITE);
        paint.setTextAlign(Paint.Align.CENTER);
        paint.setTextSize(100);
        canvas.drawText("Welcome!",520,1238,paint);
        canvas.drawText("Tap to play.",520,1350,paint);    }
}
