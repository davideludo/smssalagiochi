package sms1920.lisi_ludovico.salagiochi.tetris;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.RectF;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.RelativeLayout;

import com.lisi_ludovico.salagiochi.R;

public class Tetris extends Activity {
    TetrisCtrl mTetrisCtrl;
    Point mScreenSize = new Point(0, 0);
    Point mStartPos = new Point(-1, -1);
    Point mFinalPos=new Point(-1,-1);
    int mCellSize = 0;
    boolean mIsTouchMove = false;
    String nickname;
    Boolean ospite;
    Button btdown;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        //requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_tetris);
        DisplayMetrics dm = this.getApplicationContext().getResources().getDisplayMetrics();
        mScreenSize.x = dm.widthPixels;
        mScreenSize.y = dm.heightPixels;
        mCellSize = (int)(mScreenSize.x / 8);
/*
        if(user==null) {
            Intent i;
            i = getIntent();
            nickname = i.getStringExtra("nick");
            ospite = i.getBooleanExtra("ospite", true);
        }

 */

        initTetrisCtrl();


    }
    public static int screenWidth() {
        return Resources.getSystem().getDisplayMetrics().widthPixels;
    }

    void initTetrisCtrl() {
        mTetrisCtrl = new TetrisCtrl(this);
        for(int i=0; i <= 7; i++) {
            Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.cell0 + i);
            mTetrisCtrl.addCellImage(i, bitmap);
        }
        RelativeLayout layoutCanvas = findViewById(R.id.layoutCanvas);
        layoutCanvas.addView(mTetrisCtrl);
    }



    public boolean onTouchEvent(MotionEvent event) {
        super.onTouchEvent(event);
        switch( event.getAction() ) {
            case MotionEvent.ACTION_DOWN :
                mIsTouchMove = false;
                if( event.getY() < (int)(mScreenSize.y * 0.75)) {
                    mStartPos.x = (int) event.getX();
                    mStartPos.y = (int) event.getY();
                }
                break;
            case MotionEvent.ACTION_UP :
                mFinalPos.x= (int) event.getX();
                mFinalPos.y= (int) event.getY();

                if (mFinalPos.y > mStartPos.y - 200 && mFinalPos.y < mStartPos.y + 200) {
                    if (mFinalPos.x > mStartPos.x + screenWidth()/6) {
                        mTetrisCtrl.block2Right();
                        mStartPos.x = mFinalPos.x;
                    } else if (mFinalPos.x < mStartPos.x - screenWidth()/6) {
                        mTetrisCtrl.block2Left();
                        mStartPos.x = mFinalPos.x;
                    }
                } else if (mFinalPos.x > mStartPos.x - 200 && mFinalPos.x < mStartPos.x + 200){
                    if (mFinalPos.y > mStartPos.y + screenWidth()/6) {
                        mTetrisCtrl.block2Bottom();
                        mStartPos.y = mFinalPos.y;
                    } else if (mFinalPos.y < mStartPos.y - screenWidth()/6) {
                        mTetrisCtrl.block2Rotate();
                        mStartPos.y = mFinalPos.y;
                    }
                }

                break;
        }
        return true;
    }

    @Override
    protected void onStop() {
        super.onStop();
        mTetrisCtrl.pauseGame();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        mTetrisCtrl.restartGame();
    }
}
